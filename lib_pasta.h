#ifndef LABEL_MATCHER__LIB_PASTA_H_
#define LABEL_MATCHER__LIB_PASTA_H_

#include <complex>
#include <vector>
// Link is required as of g++ 8.3.0: -lstdc++fs
#include <filesystem>

// T: float, double, long double
template<class T>
std::complex<T> MaxCoverCircle(std::vector<std::complex<T>> &p, T radius) {
  const int n = p.size();
  const T eps = 1e-6;
  int num_points = 1;
  std::complex<T> center_max(p[0]);

  for (int i = 0; i < n; ++i) {
    for (int j = i + 1; j < n; ++j) {
      auto diff = p[i] - p[j];
      T dist = abs(diff);

      if (dist - 2*radius > eps) continue;

      auto mid = p[j] + (diff/(T) 2);
      auto mid_unit_normal = (diff*std::complex<T>(0, 1))/dist;
      auto mid_to_center = sqrt(radius - dist*dist/(T) 4)*mid_unit_normal;

      for (int k = 0; k < 2; ++k) {
        mid_to_center *= -1;
        auto center = mid + mid_to_center;

        int cnt = 0;
        for (int l = 0; l < n; ++l) {
          auto q = p[l] - center;
          if (q.real()*q.real() + q.imag()*q.imag() - radius*radius < eps) cnt++;
        }

        if (cnt > num_points) {
          num_points = cnt;
          center_max = center;
        }
      }
    }
  }

  return center_max;
}

std::vector<std::string> ListImgPath(const std::string &root) {
  std::vector<std::string> paths;

  for (const auto &a : std::filesystem::recursive_directory_iterator(root)) {
    if (!a.is_directory()) {
      if (a.path().extension() == ".png" || a.path().extension() == ".jpg")
        paths.push_back(a.path().string());
    }
  }

  return paths;
}

#endif //LABEL_MATCHER__LIB_PASTA_H_
