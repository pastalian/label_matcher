# Label matcher
Label matcher for lab project, using OpenCV.

## Build
```
mkdir cmake-build-debug
cd cmake-build-debug
cmake ..
make
```

## Launch
```
./matcher
```
