#include <filesystem>
#include <iostream>

#include <opencv2/opencv.hpp>

#include "lib_pasta.h"

using namespace cv;
using namespace std;

// CONSTANT PARAMETERS
const string src_img = "../img/";
const int height_camera = 1080;
const int width_camera = 1920;
const double height_display = 720;
const double width_display = 1280;

const double size_train_img = 100;

const float radius = 100;
const int num_frame_skip = 50;

const int match_threshold = 10;

const double match_par = 0.6; // 対応点のしきい値

class ImageData {
 public:
  string path_;
  Mat image_;
  Mat gray_;
  Mat desc_;
  vector<KeyPoint> keys_;
};

void MatchingWithCamera();
ImageData ComputeImageData(const string &, Ptr<Feature2D> &);
void MatchQuery(Mat &, vector<ImageData> &, Ptr<Feature2D> &, Ptr<DescriptorMatcher> &, int,
                float, vector<pair<Point, string>> &);
void ExtractGoodMatch(vector<DMatch> &, vector<vector<DMatch>> &, vector<KeyPoint> &,
                      const ImageData &, Mat &);
Point ComputeMaxCoverCircle(vector<DMatch> &, vector<KeyPoint> &, float);
Point ComputeCircleCenter(vector<DMatch> &, vector<KeyPoint> &);
void GetMatchedPoints(vector<complex<float>> &, vector<DMatch> &, vector<KeyPoint> &);
void ShowMatchedPoints(vector<DMatch> &, vector<KeyPoint> &, Mat &);
void ShowKeyPoints(const Mat &, const vector<KeyPoint> &);
void ShowKeyPoints(const ImageData &);

void MatchingWithCamera() {
  VideoCapture cap(0);

  if (!cap.isOpened()) {
    cerr << "Failed to open device." << endl;
    return;
  }

  cap.set(CAP_PROP_FRAME_WIDTH, width_camera);
  cap.set(CAP_PROP_FRAME_HEIGHT, height_camera);

  auto img_paths = ListImgPath(src_img);

  Ptr<Feature2D> feature = AKAZE::create();
  Ptr<DescriptorMatcher> matcher = DescriptorMatcher::create("BruteForce-L1");
//  Ptr<DescriptorMatcher> matcher = DescriptorMatcher::create("BruteForce");
//  Ptr<DescriptorMatcher> matcher = DescriptorMatcher::create("FlannBased");

  vector<ImageData> img_data;
  img_data.reserve(img_paths.size());

  for (const auto &img_path : img_paths) {
    ImageData data = ComputeImageData(img_path, feature);
    img_data.push_back(data);
  }

  vector<pair<Point, string>> centers;
  int num_skipped = num_frame_skip;

  while (true) {
    Mat frame;
    cap.read(frame);

    if (num_skipped == num_frame_skip) {
      centers.clear();

      MatchQuery(frame, img_data, feature, matcher, match_threshold, radius, centers);

      num_skipped -= num_frame_skip;
    } else {
      num_skipped++;
    }

    for (const auto &center : centers) {
      circle(frame, center.first, (int) radius, {0, 0, 255}, 5);
      string name = center.second.substr(src_img.size());
      putText(frame, name, center.first, FONT_HERSHEY_SIMPLEX, 0.7, Scalar(255, 0, 0), 2);
    }

    imshow("frame", frame);

    if (waitKey(1) == 'q') {
      break;
    }
  }
}

ImageData ComputeImageData(const string &path, Ptr<Feature2D> &feature) {
  ImageData data;

  Mat img = imread(path);
  resize(img, data.image_, Size(), size_train_img/img.rows, size_train_img/img.cols);
  data.image_ = imread(path);
  cvtColor(data.image_, data.gray_, COLOR_BGR2GRAY);
  data.path_ = path;

  Mat desc;
  feature->detectAndCompute(data.gray_, Mat(), data.keys_, desc);
  desc.convertTo(data.desc_, CV_32FC1);

//  ShowKeyPoints(data);

  return data;
}

void MatchQuery(Mat &query, vector<ImageData> &img_data, Ptr<Feature2D> &feature,
                Ptr<DescriptorMatcher> &matcher, int threshold, float radius,
                vector<pair<Point, string>> &res_centers) {
  Mat gray, desc, desc_orig;
  vector<KeyPoint> kp;

  cvtColor(query, gray, COLOR_BGR2GRAY);

  feature->detectAndCompute(gray, Mat(), kp, desc_orig);
  desc_orig.convertTo(desc, CV_32FC1);

  // DEBUG
  ShowKeyPoints(query, kp);

  for (const auto &data : img_data) {
    vector<vector<DMatch>> matches;
    matcher->knnMatch(desc, data.desc_, matches, 2);

    Mat trans;
    vector<DMatch> inlier_matches;
    ExtractGoodMatch(inlier_matches, matches, kp, data, trans);

    cout << "Matched num: " << inlier_matches.size() << ", (threshold is " << threshold << ")" <<
         endl;

    if (inlier_matches.size() < threshold) {
      continue;
    }

    cout << "-> Matched with: " << data.path_ << endl;

    // DEBUG
    ShowMatchedPoints(inlier_matches, kp, query);

//    res_centers.emplace_back(ComputeMaxCoverCircle(inlier_matches, kp, radius), data.path_);
    res_centers.emplace_back(ComputeCircleCenter(inlier_matches, kp), data.path_);
  }
}

void ExtractGoodMatch(vector<DMatch> &res, vector<vector<DMatch>> &knn_matches,
                      vector<KeyPoint> &kp, const ImageData &data, Mat &trans) {
  vector<DMatch> matches_good;
  matches_good.reserve(knn_matches.size());

  vector<Point2f> match_point_1, match_point_2;
  match_point_1.reserve(knn_matches.size());
  match_point_2.reserve(knn_matches.size());

  for (const auto &match : knn_matches) {
    double dist_1 = match[0].distance;
    double dist_2 = match[1].distance;

    // 良い点を残す（最も類似する点と次に類似する点の類似度から）
    if (dist_1 <= dist_2*match_par) {
      matches_good.push_back(match[0]);
      match_point_1.push_back(kp[match[0].queryIdx].pt);
      match_point_2.push_back(data.keys_[match[0].trainIdx].pt);
    }
  }

  // ホモグラフィ行列推定
  Mat masks;
  if (!match_point_1.empty() && !match_point_2.empty()) {
    trans = findHomography(match_point_1, match_point_2, masks, RANSAC, 3);
  }

  // RANSACで使われた対応点のみ抽出
  res.reserve(matches_good.size());
  for (auto i = 0; i < masks.rows; ++i) {
    auto *inlier = masks.ptr<uchar>(i);
    if (inlier[0] == 1) {
      res.push_back(matches_good[i]);
    }
  }
}

Point ComputeMaxCoverCircle(vector<DMatch> &matches, vector<KeyPoint> &kp, float radius) {
  vector<complex<float>> p;
  GetMatchedPoints(p, matches, kp);

  auto center = MaxCoverCircle(p, radius);
  return Point((int) center.real(), (int) center.imag());
}

Point ComputeCircleCenter(vector<DMatch> &matches, vector<KeyPoint> &kp) {
  vector<complex<float>> p;
  GetMatchedPoints(p, matches, kp);

  complex<float> q;
  for(const auto &a : p) {
    q += a;
  }
  return Point((int)(q.real()/p.size()), (int)(q.imag()/p.size()));
}

void GetMatchedPoints(vector<complex<float>> &res, vector<DMatch> &matches, vector<KeyPoint> &kp) {
  res.reserve(matches.size());

  for (const auto &match : matches) {
    res.emplace_back(kp[match.queryIdx].pt.x, kp[match.queryIdx].pt.y);
  }
}

void ShowMatchedPoints(vector<DMatch> &matches, vector<KeyPoint> &kp, Mat &img) {
  vector<complex<float>> p;
  GetMatchedPoints(p, matches, kp);

  for (const auto &a : p) {
    circle(img, Point((int) a.real(), (int) a.imag()), 10, Scalar(0, 0, 255), 3);
  }

  resize(img, img, Size(), height_display/img.rows, width_display/img.cols);
  imshow("Matched points", img);
}

void ShowKeyPoints(const Mat &frame, const vector<KeyPoint> &kp) {
  Mat img;
  drawKeypoints(frame, kp, img, Scalar::all(-1),
                DrawMatchesFlags::DRAW_RICH_KEYPOINTS);
  resize(img, img, Size(), height_display/img.rows, width_display/img.cols);
  imshow("KeyPoints", img);
}

void ShowKeyPoints(const ImageData &data) {
  Mat img;
  drawKeypoints(data.image_, data.keys_, img, Scalar::all(-1),
                DrawMatchesFlags::DRAW_RICH_KEYPOINTS);
  imshow("KeyPoints", img);

  while (true) {
    if (waitKey(1) == 'q') break;
  }
}

int main(int argc, char *argv[]) {

  MatchingWithCamera();

  return 0;
}
